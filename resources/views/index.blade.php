@extends ('layouts.frame')
@section ('title', 'Home')
@section ('content')
<section class="hero is-success is-fullheight">
    <div class="hero-head">
        @include ('layouts.nav')
    </div>

    <div class="hero-body">
        <div class="container has-text-centered">
            <p class="title is-spaced" style="font-size: 1000%;">
                Kazumi
            </p>
        </div>
    </div>

    <div class="hero-foot">
        @include ('layouts.navbottom')
        </div>
</section>
@endsection
